using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTrafficLight.Context;
using TestTrafficLight.Dtos;
using TestTrafficLight.Logics;

namespace TestTrafficLight.Controllers
{
    public class SequenceController: BaseController
    {
        private readonly SequenceLogic _logic;

        public SequenceController(SequenceLogic logic)
        {
            _logic = logic;
        }

        [HttpPost]
        public async Task<ApiResult<SequenceDto>> Create()
        {
            try
            {
                return ApiResult.Ok(await _logic.Create());
            }
            catch (Exception ex)
            {
                return ExceptionResult<SequenceDto>(ex);
            }
        }
    }
}