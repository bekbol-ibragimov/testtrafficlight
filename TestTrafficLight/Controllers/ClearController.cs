using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTrafficLight.Dtos;
using TestTrafficLight.Logics;

namespace TestTrafficLight.Controllers
{
    [Route("api/[controller]")]
    public class ClearController: BaseController
    {
        private readonly SequenceLogic _logic;

        public ClearController(SequenceLogic logic)
        {
            _logic = logic;
        }

        [HttpGet]
        public async Task<ApiResult<string>> Clear()
        {
            try
            {
                return ApiResult.Ok(await _logic.ClearAll());
            }
            catch (Exception ex)
            {
                return ExceptionResult<string>(ex);
            }
        }
    }
}