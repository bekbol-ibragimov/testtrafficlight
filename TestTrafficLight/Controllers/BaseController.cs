using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using Serilog;
using System.Net;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Globalization;
using TestTrafficLight.Dtos;

namespace TestTrafficLight.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
//    [Authorize]
    public class BaseController: Controller
    {
        private int _userId = 0;
        private string _lang = "ru";
        protected int CurrentUserId
        {
            get
            {
                if (_userId != 0) return _userId;
                var claimValue = User
                    .Claims
                    .FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Sub)?
                    .Value;
                _userId = int.Parse(claimValue);

                return _userId;
            }
        }
        

        protected string getCurrentCulture
        {
            get
            {
                if (Request != null)
                {
                    string lang = Request.Headers["Accept-Language"];
                    string[] langs = new string[] { "ru", "kz", "en" };
                    if (langs.Contains(lang))
                        _lang = lang;
                }
                    
                return _lang;
            }
        }

        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            CultureInfo.CurrentCulture = new CultureInfo(getCurrentCulture);
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(ApiResult.Error(context.ModelState));
            }
        }

        protected ApiResult<T> ExceptionResult<T>(Exception ex, object args = null)
        {
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;
            var msg = $"{controllerName} {actionName} {ex.Message}";

            var res = ApiResult<T>.Error(ex);

            if (ex is ArgumentException)
            {
                Log.Warning(ex, msg, args);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return res;
            }

            Log.Error(ex, msg, args);
            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return res;
        }

//        protected IActionResult ExceptionResult(Exception ex, params object[] args)
//        {
//            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
//            var actionName = ControllerContext.ActionDescriptor.ActionName;
//            var msg = $"{controllerName} {actionName} {ex.Message}";
//
//            var res = ApiResult.Error(ex);
//            if (ex is ArgumentException)
//            {
//                Log.Warning(ex, msg, args);
//                return BadRequest(res);
//            }
//
//            Log.Error(ex, msg, args);
//            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
//            return Json(res);
//        }

//        protected JsonResult ExceptionJsonResult(Exception ex, object args = null)
//        {
//            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
//            var actionName = ControllerContext.ActionDescriptor.ActionName;
//            var msg = $"{controllerName} {actionName} {ex.Message}";
//            var res = ApiResult.Error(ex);
//
//            if (ex is ArgumentException)
//            {
//                Log.Warning(ex, msg, args);
//                Response.StatusCode = (int)HttpStatusCode.BadRequest;
//            }
//            else
//            {
//                Log.Error(ex, msg, args);
//                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
//            }
//            return Json(res);
//        }

    }
}
