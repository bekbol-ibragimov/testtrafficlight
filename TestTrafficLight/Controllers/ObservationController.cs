using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTrafficLight.Dtos;
using TestTrafficLight.Logics;

namespace TestTrafficLight.Controllers
{
    public class ObservationController:BaseController
    {
        private readonly ObservationLogic _logic;

        public ObservationController(ObservationLogic logic)
        {
            _logic = logic;
        }

        [HttpPost]
        public async Task<ApiResult<ObservationAddDtoOut>> Add(ObservationAddDtoIn dto)
        {
            
            try
            {
                return ApiResult.Ok(await _logic.Add(dto));
            }
            catch (Exception ex)
            {
                return ExceptionResult<ObservationAddDtoOut>(ex);
            }
        }
    }
}