import {Component, OnInit} from '@angular/core';
import {AppService} from './app.service';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {


  rightNumber1 = [
    { label: 0, checked: true },
    { label: 1, checked: true },
    { label: 2, checked: true },
    { label: 3, checked: true },
    { label: 4, checked: true },
    { label: 5, checked: true },
    { label: 6, checked: true }
  ];

  rightNumber2 = [
    { label: 0, checked: true },
    { label: 1, checked: true },
    { label: 2, checked: true },
    { label: 3, checked: true },
    { label: 4, checked: true },
    { label: 5, checked: true },
    { label: 6, checked: true }
  ];



  isCollapsed = false;
  isReverseArrow = false;
  width = 200;
  startTimer = 4;
  timer = 0;
  isRed = true;
  data1;
  data2;
  intervalId;
  sequence;
  pause = false;
  startData = {
    0: '1110111',
    1: '0010010',
    2: '1011101',
    3: '1011011',
    4: '0111010',
    5: '1101011',
    6: '1101111',
    7: '1010010',
    8: '1111111',
    9: '1111011',
  }
  constructor(public appService: AppService,
              private message: NzMessageService){

  }
  ngOnInit() {

  }


  startLight(){
    this.data1 = Object.assign({}, this.startData);
    this.data2 = Object.assign({}, this.startData);
    for (let i = 0; i < 10; i++) {
      this.data1[i] = this.convertData(this.data1[i], this.rightNumber1);
      this.data2[i] = this.convertData(this.data2[i], this.rightNumber2);
    }


    this.appService.sequenceCreate().subscribe((res: any) => {
      this.sequence = res.response.sequence;
      this.isRed = false;
      this.timer = this.startTimer;
      clearInterval(this.intervalId);
      this.intervalTimer();
    });

  }
  intervalTimer(){
    this.pause = false;
    this.intervalId = setInterval(() => {
      console.log(this.timer);
      let data = {sequence: this.sequence,
        observation: {color: this.isRed? 'red': "green",
          numbers: this.getNumbers()}};
      this.appService.observationAdd(data).subscribe((result: any) => {

      });
      if (this.timer === 0) {
        clearInterval(this.intervalId);
      }
      if (this.timer === 1) {
        this.isRed = !this.isRed;
      }
      this.timer += -1;
    }, 1000);
  }

  setPause(){
    clearInterval(this.intervalId);
    this.pause = true;
  }


  convertData(crypt: string, rightNumber): string{
    let offs = rightNumber.filter( x => x.checked == false);
    offs.forEach( x => {
      crypt =  crypt.substring(0, x.label) + 'h' + crypt.substring(x.label + 1);
    })
    return  crypt;
  }


  numberToString(n){
    return n > 9 ? "" + n: "0" + n;
  }

  getNumbers(): string[]{
    let num1 = Number( this.numberToString(this.timer).substr(0,1) );
    let num2 = Number( this.numberToString(this.timer).substr(1,1) );
    let numbers = [ this.data1[num1], this.data1[num2]];
    return numbers;
  }

  numberToElement(p,n){
    if(this.isRed) return false;
    let number = Number( this.numberToString(this.timer).substr(p,1) );
    let result;
    if (p == 0){
      result = this.data1[number].substr(n, 1) === '1';
    } else {
      result = this.data2[number].substr(n, 1) === '1';
    }
    return result;
  }



  clearAll(){
    this.appService.clearAll().subscribe((res:any) => {
      this.message.info('Данные успешно очищенны');
    });
  }




}
