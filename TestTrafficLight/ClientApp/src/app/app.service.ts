import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class AppService {
  showSpinner = false;

  constructor(private http: HttpClient) {
  }



  clearAll( ) {
    return this.http.get('api/Clear');
  }

  sequenceCreate() {
    return this.http.post('api/sequence/create', {});
  }


  observationAdd(data) {
    return this.http.post('api/observation/add', data);
  }
}
