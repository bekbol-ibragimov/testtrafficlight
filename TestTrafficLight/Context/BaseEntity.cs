using System;
using System.ComponentModel.DataAnnotations;

namespace TestTrafficLight.Context
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        
        /// <summary>
        /// Дата создания сущности
        /// </summary>
        public DateTime DateCreate { get; set; } = DateTime.Now;
        
        /// <summary>
        /// Дата изменения сущности
        /// </summary>
        public DateTime DateUpdate { get; set; } = DateTime.Now;

    }
}