using System;
using System.Collections.Generic;

namespace TestTrafficLight.Context
{
    public class User: BaseEntity
    {
        public Guid Guid { get; set; }
        public int [] Start { get; set; }
        public int [] Start1 { get; set; }
        public int [] Start2 { get; set; }
        public string Missing1 { get; set; } = "0000000";
        public string Missing2 { get; set; } = "0000000";
        public ICollection<Light> Lights { get; set; }
    }
}