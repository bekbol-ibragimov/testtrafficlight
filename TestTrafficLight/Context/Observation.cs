using System.Collections.Generic;

namespace TestTrafficLight.Context
{
    public class Observation
    {
        public string Color { get; set; }

        public List<string> Numbers { get; set; }
    }
}