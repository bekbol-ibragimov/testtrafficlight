namespace TestTrafficLight.Context
{
    public class Encryption
    {
        public int Num { get; set; }
        public string Cipher { get; set; }

        public bool IsPossibly { get; set; } = true;
    }
}