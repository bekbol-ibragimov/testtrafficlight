using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace TestTrafficLight.Context
{
    public class AppTestContext : DbContext
    {


        public AppTestContext()
        {
        }

        public AppTestContext(DbContextOptions<AppTestContext> options)
            : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Light> Lights { get; set; }


//        protected override void OnModelCreating(ModelBuilder modelBuilder)
//        {
//            modelBuilder.Entity<User>()
//                .Property(e => e.Missing)
//                .HasConversion(
//                    s => JsonConvert.SerializeObject(s),
//                    s => JsonConvert.DeserializeObject<string[]>(s)
//                );
//        }
    }
}