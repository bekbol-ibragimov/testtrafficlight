using System.Collections.Generic;

namespace TestTrafficLight.Context
{
    public class ResponseObservation
    {
        public List<int> Start { get; set; }
        public List<string> Missing { get; set; }
    }
}