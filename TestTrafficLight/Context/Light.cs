namespace TestTrafficLight.Context
{
    public class Light: BaseEntity
    {
        public int UserId { get; set; }
        public string[] Numbers { get; set; }
        public string Color { get; set; }
        public int Index { get; set; }
    }
}