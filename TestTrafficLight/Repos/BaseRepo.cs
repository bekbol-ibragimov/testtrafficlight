using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Remotion.Linq.Clauses;
using TestTrafficLight.Context;

namespace TestTrafficLight.Repos
{
    public class BaseRepo<TEntity> where TEntity : BaseEntity
    {
        public AppTestContext Context { get; }
        public DbSet<TEntity> Repo { get; }

        public BaseRepo(AppTestContext context)
        {
            Context = context;
            Repo = Context.Set<TEntity>();
        }

//        public async Task<Context.Auth.User> GetCurrentUser(Expression<Func<Context.Auth.User, bool>> expression)
//        {
//            return await Context.AuthUsers.Where(expression).FirstOrDefaultAsync();
//        }

        public async Task<TEntity> Add(TEntity entity)
        {
            await Repo.AddAsync(entity);
            await Save();
            return entity;
        }
        public async Task<IEnumerable<TEntity>> AddRange(IEnumerable<TEntity> entities)
        {
            var resultEntities = new List<TEntity>();
            foreach (var entity in entities)
            {
                await Repo.AddAsync(entity);
                resultEntities.Add(entity);
            }
            await Save();
            return resultEntities;
        }

        public async Task Save()
        {
            await Context.SaveChangesAsync();
        }

//        public async Task UpdateRange(Expression<Func<TEntity, bool>> expression, TEntity entity)
//        {
//            await Repo.Where(expression).UpdateAsync(x => entity);
//            await Save();
//        }

        public virtual async Task<TEntity> GetById(int id)
        {
            return await Repo.FindAsync(id);
        }

        public async Task Delete(TEntity entity)
        {
            Repo.Remove(entity);
            await Save();
        }
        
        public async Task DeleteRange(IEnumerable<TEntity> entities)
        {
            Repo.RemoveRange(entities);
//            foreach (var e in entities)
//            {
//                Repo.Remove(e);
//            }
            await Save();
        }

        public async Task Update(TEntity entity)
        {
            Repo.Update(entity);
            await Save();
        }


        public async Task UpdateRange(IEnumerable<TEntity> entities)
        {
            Repo.UpdateRange(entities);
            Context.SaveChanges();
        }

        public async Task AddOrUpdate(TEntity entity)
        {
            var entry = Context.Entry(entity);
            switch (entry.State)
            {
                case EntityState.Detached:
                    await Repo.AddAsync(entity);
                    break;
                case EntityState.Modified:
                    Repo.Update(entity);
                    break;
                case EntityState.Added:
                    await Repo.AddAsync(entity);
                    break;
                case EntityState.Unchanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public async Task<IEnumerable<TEntity>> Select(Expression<Func<TEntity, bool>> expression)
        {
            return await Repo.Where(expression).ToListAsync();
        }

        public IQueryable<TEntity> SelectQuery(Expression<Func<TEntity, bool>> expression)
        {
            return Repo.Where(expression);
        }
    }
}
