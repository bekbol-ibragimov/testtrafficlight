using TestTrafficLight.Context;

namespace TestTrafficLight.Repos
{
    public class LightRepo: BaseRepo<Light>
    {
        public LightRepo(AppTestContext context) : base(context)
        {
        }
    }
}