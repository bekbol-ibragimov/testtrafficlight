using TestTrafficLight.Context;

namespace TestTrafficLight.Repos
{
    public class UserRepo: BaseRepo<User>
    {
        public UserRepo(AppTestContext context) : base(context)
        {
        }
    }
}