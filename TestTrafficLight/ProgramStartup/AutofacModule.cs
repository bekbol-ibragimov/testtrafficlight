using System.Reflection;
using Autofac;

namespace TestTrafficLight.ProgramStartup
{
    public class AutofacModule: Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = Assembly.Load(new AssemblyName("TestTrafficLight"));
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Repo"));
            assembly = Assembly.Load(new AssemblyName("TestTrafficLight"));
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Logic"));
        }
    }
}