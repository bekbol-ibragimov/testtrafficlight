using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestTrafficLight.Context;
using TestTrafficLight.Dtos;
using TestTrafficLight.Repos;

namespace TestTrafficLight.Logics
{
    public class SequenceLogic: BaseLogic
    {
        private readonly UserRepo _repo;

        public SequenceLogic(UserRepo repo)
        {
            _repo = repo;
        }
        
        public async Task<SequenceDto> Create()
        {
            var entity = new User(){ Guid = Guid.NewGuid()};
            await _repo.Add(entity);

            var sequence = new SequenceDto() {Sequence = entity.Guid};
            return sequence;
        }
        
        public async Task<string> ClearAll()
        {
            var users = await _repo.SelectQuery(x => true).ToListAsync();
            await _repo.DeleteRange(users);
            return "ok";
        }
    }
}