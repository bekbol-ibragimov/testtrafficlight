using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestTrafficLight.Context;
using TestTrafficLight.Dtos;
using TestTrafficLight.Repos;

namespace TestTrafficLight.Logics
{
    public class ObservationLogic: BaseLogic
    {
        private readonly UserRepo _repo;

        public ObservationLogic(UserRepo repo)
        {
            _repo = repo;
        }
        
        public async Task<ObservationAddDtoOut> Add(ObservationAddDtoIn dto)
        {
            var user = await _repo.SelectQuery(x => x.Guid.ToString() == dto.Sequence)
                .Include(x => x.Lights)
                .FirstOrDefaultAsync();
            
            if (user == null) 
                throw new ArgumentException("“The sequence isn't found” - последовательность не найдена или не была создана");

            if (user.Lights.Any())
            {
                var last = user.Lights.Last();
                if(last.Color == "red" )
                    throw new ArgumentException("“The red observation should be the last” - была попытка отправить данные после наблюдения красного индикатора");
                
//                if(last.Color == dto.Observation.Color && last.Numbers == dto.Observation.Numbers)
//                    throw new ArgumentException("“No solutions found” -  Значения были по ошибке отправлены дважды");
                
            }
            else
            {
                if(dto.Observation.Color == "red")
                    throw new ArgumentException("“There isn't enough data” - первое же наблюдение сигнализирует о красном цвете индикатора");
            }

            var count = user.Lights.Count();

            int[] start;
            
            if (dto.Observation.Color == "red")
            {
                if (!user.Start.Contains(count))
                    throw new ArgumentException("“No solutions found” -  не удаётся найти подходящее решение");
                    
                start = new []{ count };
            }
            else
            {
                var st1 = user.Start== null ? null: GetStartElement(user.Start, count, 0);
                var st2 = user.Start== null ? null: GetStartElement(user.Start, count, 1);

                (user.Start1, user.Missing1) = AlgorithmStartMissing(dto.Observation.Numbers[0], st1, user.Missing1);
                (user.Start2, user.Missing2) = AlgorithmStartMissing(dto.Observation.Numbers[1], st2, user.Missing2);
            
                if(user.Start1.Length == 0 || user.Start1.Length == 0) 
                    throw new ArgumentException("“No solutions found” -  не удаётся найти подходящее решение");
                start = (from s1 in user.Start1 from s2 in user.Start2 select s1 * 10 + s2 + count).ToArray();
                
            }
            
            var light = new Light()
            { 
                UserId = user.Id,
                Numbers = dto.Observation?.Numbers, 
                Color = dto.Observation.Color,
                Index = user.Lights.Count() + 1
            };
            user.Start = start;
            user.Lights.Add(light);
            await _repo.Update(user);

            var result = new ObservationAddDtoOut() {Start = start, Missing = new List<string>(){user.Missing1, user.Missing2}};

            return result;
        }

        private (int[], string) AlgorithmStartMissing(string number, int[] lastStarts, string missing)
        {
            var data = getNewData();

            var encrypts = data;
            if (lastStarts != null) encrypts = data.Where(x => lastStarts.Contains(x.Num)).ToList();
            
            for (int i = 0; i < 7; i++)
            {
                if (number.Substring(i, 1) != "1") continue;
                var notContains = encrypts.Where(x => x.Cipher.Substring(i, 1)=="0" && x.IsPossibly == true).ToList();
                foreach (var notContain in notContains)
                {
                    notContain.IsPossibly = false;
                }
            }

            var contains = encrypts.Where(x => x.IsPossibly == true).ToList();
            var resultMissing =  missing;
            
            for (int i = 0; i < 7; i++)
            {
                if (number.Substring(i, 1) == "0" && resultMissing.Substring(i,1) == "0")
                {
                    if (!(encrypts.Any(x => x.Cipher.Substring(i, 1) == "0" && x.IsPossibly == true)))
                    {
                        resultMissing = replaceString(resultMissing, i, '1');
                    }
                }

            }

            return (contains.Select(x => x.Num).ToArray(), resultMissing);
        }

        private string replaceString(string value, int index, char element)
        {
            char[] array = value.ToCharArray();
            array[index] = element;
            var result = new string(array);
            return result;
        }

        private List<Encryption> getNewData()
        {
            return new List<Encryption>
            {
                new Encryption() {Num = 0, Cipher = "1110111"},
                new Encryption() {Num = 1, Cipher = "0010010"},
                new Encryption() {Num = 2, Cipher = "1011101"},
                new Encryption() {Num = 3, Cipher = "1011011"},
                new Encryption() {Num = 4, Cipher = "0111010"},
                new Encryption() {Num = 5, Cipher = "1101011"},
                new Encryption() {Num = 6, Cipher = "1101111"},
                new Encryption() {Num = 7, Cipher = "1010010"},
                new Encryption() {Num = 8, Cipher = "1111111"},
                new Encryption() {Num = 9, Cipher = "1111011"},
            };
        }

        private int[] GetStartElement(int[] start, int count, int startIndex)
        {
            return start.Select(x => Convert.ToInt32((x - count).ToString("00").Substring(startIndex,1))).Distinct().ToArray();
        }
    }
}