using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TestTrafficLight.Dtos
{
    public class ApiResult<T>
    {
        public T Response { get; set; }
        public string Status { get; set; }
        
        public Exception Msg { get; set; }
        public List<Error> Errors { get; set; } = new List<Error>();

        
        public new static ApiResult<T> Ok()
        {
            return new ApiResult<T>()
            {
                Status = "ok",
            };
        }
        
        public new static ApiResult<T> Ok(T response)
        {
            return new ApiResult<T>()
            {
                Response = response,
                Status =  "ok",
            };
        }
        
        public static ApiResult<T> Error(Exception exception)
        {
            var msg = exception.Message;
            if (exception.InnerException != null)
            {
                msg = exception.InnerException.Message;
            }
            var res = new ApiResult<T>()
            {
                Status = "error",
                Msg = exception,
            };
            return res;
        }
        
        public static ApiResult<T> Error(ModelStateDictionary modelState)
        {
            var res = new ApiResult<T>()
            {
                Status = "ok",
                Errors = new List<Error>() {},
            };
            res.Errors = modelState.Keys.SelectMany(key =>
            {
                return modelState[key].Errors.Select(e => new Error()
                {
                    Message = e.ErrorMessage,
                    Property = key
                });
            }).ToList();
            return res;
        }
        
        public static ApiResult<T> Error(string error, string property = null)
        {
            return new ApiResult<T>()
            {
                Status = "ok",
                Errors = new List<Error>()
                {
                    new Error()
                    {
                        Message = error,
                        Property =  property
                    }
                },
            };
        }

    }
    
    public class ApiResult : ApiResult<object>
    {
        public new static ApiResult Ok()
        {
            return new ApiResult()
            {
                Status = "ok",
            };
        }
        
        public static ApiResult<T> Ok<T>(T result)
        {
            return new ApiResult<T>()
            {
                Status = "ok",
                Response = result,
            };
        }
        
        public new static ApiResult Error(Exception exception)
        {
            var msg = exception.Message;
            if (exception.InnerException != null)
            {
                msg = exception.InnerException.Message;
            }
            var res = new ApiResult()
            {
                Status = "ok",
                Msg = exception
            };
            return res;
        }

        public new static ApiResult Error(ModelStateDictionary modelState)
        {
            var res = new ApiResult()
            {
                Status = "ok",
                Errors = new List<Error>() {},
            };
            res.Errors = modelState.Keys.SelectMany(key =>
            {
                return modelState[key].Errors.Select(e => new Error()
                {
                    Message = e.ErrorMessage,
                    Property = key
                });
            }).ToList();
            return res;
        }
        
        public new static ApiResult Error(string error, string property = null)
        {
            return new ApiResult()
            {
                Status = "ok",
                Errors = new List<Error>()
                {
                    new Error()
                    {
                        Message = error,
                        Property =  property
                    }
                },
            };
        }
    }   
}