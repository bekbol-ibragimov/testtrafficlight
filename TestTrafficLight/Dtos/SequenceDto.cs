using System;

namespace TestTrafficLight.Dtos
{
    public class SequenceDto
    {
        public Guid Sequence { get; set; }
    }
}