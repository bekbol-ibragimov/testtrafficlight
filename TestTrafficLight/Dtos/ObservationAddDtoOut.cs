using System.Collections.Generic;

namespace TestTrafficLight.Dtos
{
    public class ObservationAddDtoOut
    {
        public int[] Start { get; set; }
        public List<string> Missing { get; set; }
    }
}