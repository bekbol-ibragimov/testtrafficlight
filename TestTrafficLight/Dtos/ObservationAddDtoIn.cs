using System;

namespace TestTrafficLight.Dtos
{
    public class ObservationAddDtoIn
    {
        public Observation Observation { get; set; }
        public string Sequence { get; set; } 
    }
}