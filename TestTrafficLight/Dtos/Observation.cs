namespace TestTrafficLight.Dtos
{
    public class Observation
    {
        public string Color { get; set; }
        public string [] Numbers { get; set; }
    }
}